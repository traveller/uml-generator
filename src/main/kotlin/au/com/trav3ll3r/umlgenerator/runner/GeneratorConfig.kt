package au.com.trav3ll3r.umlgenerator.runner

import java.io.File
import java.nio.file.Paths

class GeneratorConfig private constructor(
    val showHelp: Boolean,
    val generateConfig: Boolean,
    private val configFile: String?,
    val parseRootPath: String,
    val outputFilePath: String,
    val renderDependencies: Boolean,
    val renderExtends: Boolean,
    val excludeDependencies: List<String>,
    val renderRootPackage: String,
    val renderPublicMethods: Boolean
) {
    private val version: String = "v0.1"

    class Builder {
        private var showHelp: Boolean = false
        private var generateConfig: Boolean = false
        private var configFile: String? = null
        private var rootPath: String = Paths.get("").toAbsolutePath().toString()
        private var outputFile: String = "output.puml"
        private var renderDependencies: Boolean = false
        private var renderExtends: Boolean = true
        private val excludeDependencies = mutableListOf<String>()
        private var renderRootPackage: String = ""
        private var renderPublicMethods = true

        fun showHelp(): Builder {
            showHelp = true
            return this
        }

        fun generateConfig(): Builder {
            generateConfig = true
            return this
        }

        fun fromConfig(filename: String): Builder {
            configFile = filename
            return this
        }

        fun scanFilesIn(path: String): Builder {
            rootPath = path
            return this
        }

        fun outputTo(outputFile: String): Builder {
            this.outputFile = outputFile
            return this
        }

        fun withDependencies(render: String): Builder {
            renderDependencies = render=="true"
            return this
        }

        fun withInherits(render: String): Builder {
            renderExtends = render=="true"
            return this
        }

        fun excludeDependencyTo(dependencies: String): Builder {
            excludeDependencies.addAll(dependencies.split(','))
            return this
        }

        fun renderOnlySubpackagesOf(rootPackage: String): Builder {
            renderRootPackage = rootPackage
            return this
        }

        fun withPublicMethods(include: Boolean): Builder {
            renderPublicMethods = include
            return this
        }

        fun build(): GeneratorConfig {
            return GeneratorConfig(
                showHelp,
                generateConfig,
                configFile,
                rootPath,
                outputFile,
                renderDependencies,
                renderExtends,
                excludeDependencies,
                renderRootPackage,
                renderPublicMethods
            )
        }
    }

    fun printRunConfig() {
        println("UmlGenerator")
        println("  Version: $version")
        println("  Config: ${configFile ?: "[none]"}")
        println("  Root: $parseRootPath")
        println("  Output: $outputFilePath")
        println("  With dependencies: $renderDependencies")
        println("  With extends: $renderExtends")
        println("  Exclude: $excludeDependencies")
        println("  Top subpackage: $renderRootPackage")
        println("")
    }

    fun generateConfig() {
        File("default.config").outputStream().use {
            it.write(
                """
                output=output.puml
                dependencies=false
                extends=true
                exclude-dependencies=
                top-subpackage=
                """.trimIndent().toByteArray())
            it.flush()
        }
    }

    fun printHelp() {
        println("UmlGenerator $version")
        println("")
        println("Example:")
        println("generator.jar path=project/src dependencies=true")
        println("")
        println("Options:")
        println("  config=<filepath>")
        println("           config file defining options and values")
        println("           default: <default.config>")
        println("  generate-config")
        println("           creates (or overwrites) a 'default.config' file with default values")
        println("  path=<path>")
        println("           set the root path for file parsing")
        println("           default: <current directory>")
        println("  output=<file>")
        println("           set output file name (full path)")
        println("           default: output.puml")
        println("  dependencies=[true|false]")
        println("           render dependency links if set to 'true'")
        println("           default: false")
        println("  extends=[true|false]")
        println("           render 'inherits' ('extends') links if set to 'true'")
        println("           default: true")
        println("  exclude-dependencies=[package,package]")
        println("           do not render links that target dependencies starting with any in this list")
        println("           default: empty")
        println("  renderRootPackage=[package]")
        println("           render only packages/classes that start with specified [package]")
        println("           default: empty")
        println("  help")
        println("           show help")
    }
}
