package au.com.trav3ll3r.umlgenerator.runner

import java.io.File

class ConfigLoader {

    private val builder = GeneratorConfig.Builder()

    fun apply(arguments: Array<String>): GeneratorConfig.Builder {
        val cliArgs = arguments.toMutableList()
        if (cliArgs.firstOrNull { it.startsWith("config=") } == null ) {
            cliArgs.add("config=default.config")
        }

        handleArguments(cliArgs)

        return builder
    }

    private fun handleArguments(arguments: List<String>) {
        handleArgument(arguments,"help")
        handleArgument(arguments,"generate-config")
        handleArgument(arguments,"config=")
        handleArgument(arguments,"path=")
        handleArgument(arguments,"output=")
        handleArgument(arguments,"dependencies=")
        handleArgument(arguments,"inherits=")
        handleArgument(arguments,"exclude-dependencies=")
        handleArgument(arguments,"top-subpackage=")
    }

    private fun handleArgument(arguments: List<String>, argKey: String) {
        arguments.firstOrNull{ it.startsWith(argKey) }?.apply {
            val tokens = split('=')
            if (tokens.size == 2) {
                applyArgument(tokens[0], tokens[1])
            } else {
                applyArgument(tokens[0], "")
            }
        }
    }

    /**
     * @return boolean whether to continue or stop
     */
    private fun applyArgument(key: String, value: String) {
        when (key) {
            "help" -> builder.showHelp()
            "generate-config" -> builder.generateConfig()
            "config" -> loadFromFile(value)
            "path" -> builder.scanFilesIn(value)
            "output" -> builder.outputTo(value)
            "dependencies" -> builder.withDependencies(value)
            "inherits" -> builder.withInherits(value)
            "exclude-dependencies" -> builder.excludeDependencyTo(value)
            "top-subpackage" -> builder.renderOnlySubpackagesOf(value)
        }
    }

    private fun loadFromFile(filepath: String) {
        val f = File(filepath)
        if (f.exists()) {
            builder.fromConfig(filepath)
            val arguments = f.readLines()
            handleArguments(arguments)
        }
    }
}