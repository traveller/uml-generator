package au.com.trav3ll3r.umlgenerator.runner

import au.com.trav3ll3r.umlgenerator.generator.UmlGenerator

fun main(args: Array<String>) {

    val config = ConfigLoader().apply(args).build()

    checkConfig(config) {
        UmlGenerator().apply {
            buildDocument(config)
            render(config)
        }
    }
}

private fun checkConfig(config: GeneratorConfig, block: () -> Unit) {
    if (config.generateConfig) {
        config.generateConfig()
    } else if (config.showHelp) {
        config.printHelp()
    } else {
        config.printRunConfig()
        block.invoke()
        println("Finished")
    }
}
