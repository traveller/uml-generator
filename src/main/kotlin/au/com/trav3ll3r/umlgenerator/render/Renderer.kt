package au.com.trav3ll3r.umlgenerator.render

import au.com.trav3ll3r.umlgenerator.model.UmlClass
import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import au.com.trav3ll3r.umlgenerator.model.UmlPackage

interface Renderer {

    fun renderHeader(umlDocument: UmlDocument): String

    fun renderFooter(umlDocument: UmlDocument): String

    fun renderHeader(umlPackage: UmlPackage, depth: Int = 0): String

    fun renderFooter(umlPackage: UmlPackage, depth: Int = 0): String

    fun renderHeader(umlClass: UmlClass, depth: Int = 0): String

    fun renderFooter(umlClass: UmlClass, depth: Int = 0): String

    fun renderDependsOnLink(currentClass: String, dependsOn: String): String

    fun renderExtendsLink(currentClass: String, extends: String): String
}
