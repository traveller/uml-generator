package au.com.trav3ll3r.umlgenerator.render

import au.com.trav3ll3r.umlgenerator.model.UmlClass
import au.com.trav3ll3r.umlgenerator.model.UmlClass.Companion.underscoredName
import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import au.com.trav3ll3r.umlgenerator.model.UmlPackage
import au.com.trav3ll3r.umlgenerator.runner.GeneratorConfig
import java.io.File

class DocumentRenderer(
    private val config: GeneratorConfig,
    private val renderer: Renderer
) {

    private val os = File(config.outputFilePath).outputStream()
    private lateinit var umlDocument: UmlDocument

    fun render(umlDocument: UmlDocument) {
        this.umlDocument = umlDocument

        documentHeader()

        documentPackagesAndClasses()

        if (config.renderDependencies) { documentDependencies() }

        if (config.renderExtends) { documentInherits() }

        documentFooter()

        closeOutput()
    }

    private fun documentHeader() {
        w(renderer.renderHeader(umlDocument))

    }

    private fun documentFooter() {
        w(renderer.renderFooter(umlDocument))
    }

    private fun documentPackagesAndClasses() {
        umlDocument.packages.forEach {
            val item = packageHeader(it, 0)
            w(item)
        }
    }

    private fun documentDependencies() {
        val item = RenderItem()
        umlDocument.packages.forEach {
                p -> item.body.append(renderClassImportsInPackage(p))
        }
        w(item)
    }

    private fun documentInherits() {
        val item = RenderItem()
        umlDocument.packages.forEach {
                p -> item.body.append(renderClassExtendsInPackage(p))
        }
        w(item)
    }

    private fun packageHeader(umlPackage: UmlPackage, depth: Int): RenderItem {
        val item = RenderItem()
        var nextDepth = depth

        val notEmpty = !umlPackage.isEmpty()
        val includeInOutput = underscoredName(umlPackage.fullyQualifiedName)
            .startsWith(underscoredName(config.renderRootPackage))

        if (notEmpty && includeInOutput) {
            nextDepth += 2
            item.header = renderer.renderHeader(umlPackage, depth)
            item.footer = renderer.renderFooter(umlPackage, depth)
        }

        umlPackage.subpackages.forEach {
            val subItem = packageHeader(it, nextDepth)
            item.body.append(subItem.asString())
        }

        umlPackage.classes.forEach { cls ->
            val subItem: RenderItem = classBody(cls, nextDepth)
            item.body.append(subItem.asString())
        }

        return item
    }

    private fun classBody(umlClass: UmlClass, depth: Int): RenderItem {
        val result = RenderItem()
        val renderBlock = umlClass.uniqueName().startsWith(underscoredName(config.renderRootPackage))
        if (renderBlock) {
            result.header = renderer.renderHeader(umlClass, depth)
            // TODO: RENDER methods
            // result.body.append("")
            result.footer = renderer.renderFooter(umlClass, depth)
        }

        return result
    }

    /* region import links */
    private fun renderClassImportsInPackage(umlPackage: UmlPackage): String {
        val sb = StringBuilder("")

        // VISIT EACH CLASS IN PACKAGE
        umlPackage.classes.forEach { clazz -> sb.append(renderDependsOn(clazz)) }

        // VISIT ALL SUBPACKAGES
        umlPackage.subpackages.forEach { subpackage -> sb.append(renderClassImportsInPackage(subpackage)) }

        return sb.toString()
    }

    private fun renderDependsOn(umlClass: UmlClass): String {
        val sb = StringBuilder("")
        umlClass.dependsOn.forEach { edge ->
            val renderBlock = edge.startsWith(underscoredName(config.renderRootPackage))
            val notExcluded = !config.excludeDependencies.any { edge.startsWith(underscoredName(it)) }

            if (renderBlock && notExcluded) {
                sb.append(renderer.renderDependsOnLink(umlClass.uniqueName(), edge))
            }
        }
        return sb.toString()
    }
    /* endregion */

    /* region inherits links */
    private fun renderClassExtendsInPackage(umlPackage: UmlPackage): String {
        val sb = StringBuilder("")

        // VISIT EACH CLASS IN PACKAGE
        umlPackage.classes.forEach { clazz -> sb.append(renderExtendsFrom(clazz)) }

        // VISIT ALL SUBPACKAGES
        umlPackage.subpackages.forEach { subpackage -> sb.append(renderClassExtendsInPackage(subpackage)) }

        return sb.toString()
    }

    private fun renderExtendsFrom(umlClass: UmlClass): String {
        val sb = StringBuilder("")
        umlClass.extendsFrom.forEach { edge ->
            val renderBlock = edge.startsWith(underscoredName(config.renderRootPackage))
            val notExcluded = !config.excludeDependencies.any { edge.startsWith(underscoredName(it)) }

            if (renderBlock && notExcluded) {
                sb.append(renderer.renderExtendsLink(umlClass.uniqueName(), edge))
            }
        }
        return sb.toString()
    }
    /* endregion */

    private fun closeOutput() {
        os.apply {
            flush()
            close()
        }
    }

    private fun w(item: RenderItem) {
        os.write(item.asString().toByteArray(Charsets.UTF_8))
    }

    private fun w(text: String) {
        os.write(text.toByteArray(Charsets.UTF_8))
    }
}
