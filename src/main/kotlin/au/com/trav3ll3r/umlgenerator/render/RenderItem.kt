package au.com.trav3ll3r.umlgenerator.render

internal data class RenderItem(
    var header: String = "",
    var body: StringBuilder = StringBuilder(""),
    var footer: String = ""
) {

    fun asString(): String {
        return StringBuilder(header).append(body).append(footer).toString()
    }
}
