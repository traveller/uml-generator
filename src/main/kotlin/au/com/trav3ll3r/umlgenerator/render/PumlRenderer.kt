package au.com.trav3ll3r.umlgenerator.render

import au.com.trav3ll3r.umlgenerator.model.UmlClass
import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import au.com.trav3ll3r.umlgenerator.model.UmlPackage

class PumlRenderer : Renderer {

    /* region UmlDocument */
    override fun renderHeader(umlDocument: UmlDocument): String {
        return """
        @startuml
        
        !theme reddress-darkgreen
        hide empty methods
        hide empty fields
        skinparam shadowing false
        
        
        """.trimIndent()
    }

    override fun renderFooter(umlDocument: UmlDocument): String {
        return "\n@enduml"
    }
    /* endregion */

    /* region UmlPackage */
    override fun renderHeader(umlPackage: UmlPackage, depth: Int): String {
        val indent = " ".repeat(depth)
        return "${indent}package ${umlPackage.fullyQualifiedName} {\n"
    }

    override fun renderFooter(umlPackage: UmlPackage, depth: Int): String {
        val indent = " ".repeat(depth)
        return "${indent}}\n"
    }
    /* endregion */

    /* region UmlClass */
    override fun renderHeader(umlClass: UmlClass, depth: Int): String {
        val indent = " ".repeat(depth)
        return "$indent${umlClass.type} \"${umlClass.simpleName}\" as ${umlClass.uniqueName()} {\n"
    }

    override fun renderFooter(umlClass: UmlClass, depth: Int): String {
        val indent = " ".repeat(depth)
        return "${indent}}\n"
    }

    override fun renderDependsOnLink(currentClass: String, dependsOn: String): String {
        return "$currentClass --> $dependsOn\n"
    }

    override fun renderExtendsLink(currentClass: String, extends: String): String {
        return "$currentClass -up[dotted]-|> $extends\n"
    }
    /* endregion */

}
