package au.com.trav3ll3r.umlgenerator.generator

import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import au.com.trav3ll3r.umlgenerator.render.DocumentRenderer
import au.com.trav3ll3r.umlgenerator.render.PumlRenderer
import au.com.trav3ll3r.umlgenerator.render.Renderer
import au.com.trav3ll3r.umlgenerator.runner.GeneratorConfig
import java.io.File

abstract class BaseGenerator {

    /**
     * List of file extensions to include in parse step:
     * * .kt - Kotlin file
     * * .java - Java file
     */
    private val fileExtensions = listOf("kt", "java")

    protected val umlDocument = UmlDocument()

    abstract fun buildDocument(config: GeneratorConfig)

    abstract fun render(config: GeneratorConfig)

    fun getAllSupportedFiles(directory: String): List<File> {
        val f = File(directory)
        return if (f.isDirectory) {
            walkDirectory(f)
        } else emptyList()
    }

    private fun walkDirectory(dir: File): List<File> {
        val result = mutableListOf<File>()
        dir.listFiles()?.forEach { file ->
            when {
                file.isFile -> if (file.extension in fileExtensions) result.add(file)
                file.isDirectory -> result.addAll(walkDirectory(file))
                else -> {
                    println("Skipping file [${file.absolutePath}]")
                }
            }
        }

        return result
    }

    protected fun generateUml(config: GeneratorConfig) {
        val renderer: Renderer = PumlRenderer()
        DocumentRenderer(config, renderer).render(umlDocument)
    }
}
