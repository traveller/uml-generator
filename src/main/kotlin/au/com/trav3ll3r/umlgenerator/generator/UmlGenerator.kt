package au.com.trav3ll3r.umlgenerator.generator

import au.com.trav3ll3r.umlgenerator.parser.JavaFileParser
import au.com.trav3ll3r.umlgenerator.parser.KotlinFileParser
import au.com.trav3ll3r.umlgenerator.runner.GeneratorConfig
import java.io.File
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

/**
 * ALSO CHECK Kobalt (http://beust.com/kobalt/home/index.html)
 */
class UmlGenerator : BaseGenerator() {

    override fun buildDocument(config: GeneratorConfig) {
        var files: List<File> = emptyList()
        timedBlock("Finding files") {
            files = getAllSupportedFiles(config.parseRootPath)
            print(" ${files.size} files")
        }

        timedBlock("Parsing files") { parseFiles(files) }
    }

    override fun render(config: GeneratorConfig) {
        timedBlock("Rendering UML") { generateUml(config) }
    }

    private fun parseFiles(files: List<File>) {
        files.forEach { file ->
            when (file.extension) {
                "kt" -> KotlinFileParser.parseFile(file, umlDocument)
                "java" -> JavaFileParser.parseFile(file, umlDocument)
            }
        }
    }

    @OptIn(ExperimentalTime::class)
    private fun timedBlock(task: String, block: () -> Unit) {
        val start = System.currentTimeMillis()
        print("$task...")
        block.invoke()

        val d = Duration.milliseconds(System.currentTimeMillis() - start)
        val blockRun = if (d.inWholeSeconds > 2) {
            "${d.inWholeMilliseconds / 1000.0}s"
        } else {
            "${d.inWholeMilliseconds}ms"
        }

        println(" [$blockRun]")
    }
}
