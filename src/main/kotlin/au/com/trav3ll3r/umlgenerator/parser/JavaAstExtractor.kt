package au.com.trav3ll3r.umlgenerator.parser

import au.com.trav3ll3r.umlgenerator.model.UmlClass
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.ImportDeclaration
import com.github.javaparser.ast.NodeList
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.body.TypeDeclaration

class JavaAstExtractor {

    fun getPackage(astNode: CompilationUnit): String {
        return astNode.packageDeclaration.get().name.toString()
    }

    fun getClassName(astNode: CompilationUnit): UmlClass? {
        val validType = listOf("class", "interface", "enum", "object")
        astNode.childNodes.firstOrNull { it is ClassOrInterfaceDeclaration }?.let {
            if (it is ClassOrInterfaceDeclaration) {
                val className = it.nameAsString
                val type = when {
                    it.isInterface -> "interface"
                    it.isEnumDeclaration -> "enum"
                    else -> "class"
                }
                if (type in validType) {
                    return UmlClass(className, type)
                }
            }
        }

        return null
    }

    fun populateClassImports(umlClass: UmlClass, imports: NodeList<ImportDeclaration>) {
        imports.forEach { umlClass.addDependsOn(it.nameAsString) }
    }

    fun getImportList(umlClass: UmlClass, implements: NodeList<TypeDeclaration<*>>) {
        val classDeclaration = implements.firstOrNull { it is ClassOrInterfaceDeclaration } as ClassOrInterfaceDeclaration?

        val inherits = classDeclaration?.implementedTypes?.mapNotNull {
            it.nameAsString
        }?.toList()
        val resolvedInherits = resolveSimpleInheritsToCanonical(inherits!!, umlClass.dependsOn, umlClass.umlPackage!!.fullyQualifiedName)
        resolvedInherits.forEach {
            umlClass.addExtendsFrom(it)
        }
    }


    /**
     * A crude way of resolving Simple class name
     * into Canonical Name.
     */
    private fun resolveSimpleInheritsToCanonical(
        inherits: List<String>,
        imports: List<String>,
        packageName: String
    ): List<String> {
        val result = inherits.map { simpleName ->
            imports.firstOrNull { import -> import.endsWith("_$simpleName", true) }
                ?: "$packageName.$simpleName"
        }.toList()

        return result
    }
}
