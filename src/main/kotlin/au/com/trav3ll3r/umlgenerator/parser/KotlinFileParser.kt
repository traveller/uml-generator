package au.com.trav3ll3r.umlgenerator.parser

import au.com.trav3ll3r.umlgenerator.model.UmlClass
import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import kotlinx.ast.common.ast.Ast
import kotlinx.ast.common.ast.DefaultAstNode
import kotlinx.ast.common.klass.KlassDeclaration
import kotlinx.ast.grammar.kotlin.common.summary
import kotlinx.ast.grammar.kotlin.common.summary.PackageHeader
import java.io.File
import java.lang.IllegalArgumentException

object KotlinFileParser: FileParser {

    private val extractor = KotlinAstExtractor()

    @Throws(IllegalArgumentException::class)
    override fun parseFile(file: File, umlDocument: UmlDocument) {
        if (file.extension == "kt") {
            extractor.parse(file)?.let {
                addToDocument(it, umlDocument)
            }
        } else {
            throw IllegalArgumentException("Only .kt files supported")
        }
    }

    private fun addToDocument(ast: Ast, umlDocument: UmlDocument) {
        //val ast: Ast? = wrapper.parse(f)
        ast.summary(false).onSuccess { astList ->
            var packageName = ""
            var umlClass: UmlClass? = null
            val imports = mutableListOf<String>()
            val inherits = mutableListOf<String>()
            astList.forEach { fileAst ->
                when (fileAst) {
                    is PackageHeader -> packageName = extractor.getPackage(fileAst)
                    is KlassDeclaration -> {
                        umlClass = extractor.getClassName(fileAst)
                        inherits.addAll(extractor.getInheritsList(fileAst))
                    }
                    is DefaultAstNode -> imports.addAll(extractor.getImportList(fileAst))
//                        else -> println("parseFiles | AstNode | ${fileAst}")
                }
            }
            umlClass?.let {
                val resolvedInherits = resolveSimpleInheritsToCanonical(inherits, imports, packageName)
                umlDocument.addClass(packageName, umlClass!!)?.also {
                    populateClassImports(it, imports)
                    populateClassExtends(it, resolvedInherits)
                }
            }
        }.onFailure {
            println("ERROR")
        }

    }

    /**
     * A crude way of resolving Simple class name
     * into Canonical Name.
     */
    private fun resolveSimpleInheritsToCanonical(
        inherits: List<String>,
        imports: List<String>,
        packageName: String
    ): List<String> {
        val result = inherits.map { simpleName ->
            imports.firstOrNull { import -> import.endsWith(".$simpleName") }
                ?: "$packageName.$simpleName"
        }.toList()

        return result
    }

    private fun populateClassImports(umlClass: UmlClass, imports: List<String>?) {
//        println("populateClassImports | imports=${imports?.size}")
        imports?.forEach {
            umlClass.addDependsOn(it)
        }
    }

    private fun populateClassExtends(umlClass: UmlClass, extends: List<String>?) {
//        println("populateClassExtends | extends=${extends?.size}")
        extends?.forEach {
            umlClass.addExtendsFrom(it)
        }
    }
}
