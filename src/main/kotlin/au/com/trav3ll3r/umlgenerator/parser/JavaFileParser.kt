package au.com.trav3ll3r.umlgenerator.parser

import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import com.github.javaparser.JavaParser
import com.github.javaparser.ParseResult
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration
import com.github.javaparser.ast.visitor.VoidVisitorAdapter
import java.io.File
import java.lang.IllegalArgumentException

object JavaFileParser: FileParser {

    private val javaParser = JavaParser()
    private val extractor = JavaAstExtractor()

    @Throws(IllegalArgumentException::class)
    override fun parseFile(file: File, umlDocument: UmlDocument) {
        if (file.extension == "java") {
            javaParser.parse(file)?.let { parseResult ->
                addToDocument(parseResult, umlDocument)
            }
        } else {
            throw IllegalArgumentException("Only .java files supported")
        }
    }

    private fun addToDocument(parseResult: ParseResult<CompilationUnit>, umlDocument: UmlDocument) {
        try {
            object : VoidVisitorAdapter<Any?>() {
                override fun visit(astNode: CompilationUnit, arg: Any?) {
                    super.visit(astNode, arg)
                    // PACKAGE NAME
                    val packageName = extractor.getPackage(astNode)
                    // UML CLASS
                    val umlClass = extractor.getClassName(astNode)

                    umlClass?.let {
                        umlDocument.addClass(packageName, it)
                        extractor.populateClassImports(it, astNode.imports)
                        // extractor.populateClassInherits(it, astNode.types)
                        extractor.getImportList(it, astNode.types)
                    }
                }

                override fun visit(astNode: ClassOrInterfaceDeclaration, arg: Any?) {
                    super.visit(astNode, arg)
                }
            }.visit(parseResult.result.get(), null)
        } catch (ex: Exception) {
            println("error $ex")
        }
    }
}
