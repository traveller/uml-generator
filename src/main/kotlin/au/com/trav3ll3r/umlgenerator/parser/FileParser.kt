package au.com.trav3ll3r.umlgenerator.parser

import au.com.trav3ll3r.umlgenerator.model.UmlDocument
import java.io.File
import java.lang.IllegalArgumentException

interface FileParser {

    @Throws(IllegalArgumentException::class)
    fun parseFile(file: File, umlDocument: UmlDocument)
}