package au.com.trav3ll3r.umlgenerator.parser

import au.com.trav3ll3r.umlgenerator.model.UmlClass
import kotlinx.ast.common.AstSource
import kotlinx.ast.common.ast.Ast
import kotlinx.ast.common.ast.DefaultAstNode
import kotlinx.ast.common.klass.KlassDeclaration
import kotlinx.ast.common.klass.KlassIdentifier
import kotlinx.ast.common.klass.KlassInheritance
import kotlinx.ast.grammar.kotlin.common.summary.Import
import kotlinx.ast.grammar.kotlin.common.summary.PackageHeader
import kotlinx.ast.grammar.kotlin.target.antlr.kotlin.KotlinGrammarAntlrKotlinParser
import kotlinx.ast.grammar.kotlin.target.antlr.kotlin.KotlinGrammarAntlrKotlinParserExtractor
import java.io.File

class KotlinAstExtractor {

    fun parse(file: File): Ast? {
        val source = AstSource.File(file.absolutePath)
        return try {
            // print("\nParsing file: ${file.absolutePath}...")
            KotlinGrammarAntlrKotlinParser.parseKotlinFile(source)
            //KotlinGrammarAntlrKotlinParserExtractor.extract()
        } catch(ex: Exception) {
            println("Error")
            println("Exception: $ex")
            //throw Exception("Error parsing file: ${file.absolutePath}")
            null
        }
    }

    fun getPackage(astNode: PackageHeader): String {
        //println("getPackage | ${astNode.identifier.joinToString(".") { it.identifier }}")
        return astNode.identifier.joinToString(".") { it.identifier }
    }

    fun getClassName(astNode: KlassDeclaration): UmlClass? {
        val validType = listOf("class", "interface", "enum", "object")
        val identifier = astNode.identifier
        val type = astNode.keyword
        if (identifier is KlassIdentifier && type in validType) {
            return UmlClass(identifier.identifier, type)
        }

        return null
    }

    fun getInheritsList(astNode: KlassDeclaration): List<String> {
        val extends = mutableListOf<String>()
        astNode.inheritance.forEach { inheritance ->
            // TODO: EXTRACT AS CANONICAL NAME
            extends.add(inheritance.type.identifier)
        }
        return extends
    }

    fun getImportList(astNode: DefaultAstNode): List<String> {
        if (astNode.description == "importList") {
            return astNode.children.map {
                 joinKlassIdentifiers((it as Import).identifier)
            }.toList()
        }
        return emptyList()
    }

    private fun joinKlassIdentifiers(parts: List<KlassIdentifier>): String {
        return parts.joinToString(".") { it.identifier }
    }
}
