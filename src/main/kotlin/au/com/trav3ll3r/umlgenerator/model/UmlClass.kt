package au.com.trav3ll3r.umlgenerator.model

class UmlClass(
    val simpleName: String,
    val type: String = "class"
) {

    companion object {
        fun underscoredName(name: String): String {
            return name.replace('.', '_').uppercase()
        }
    }

    var umlPackage: UmlPackage? = null
        private set
    val dependsOn = mutableListOf<String>()
    val extendsFrom = mutableListOf<String>()

    fun uniqueName(): String {
        return underscoredName("%s.%s".format(umlPackage?.fullyQualifiedName, simpleName))
    }

    fun addDependsOn(uniqueName: String) {
        dependsOn.add(underscoredName(uniqueName))
    }

    fun addExtendsFrom(uniqueName: String) {
        val inherit = underscoredName(uniqueName)
        extendsFrom.add(inherit)
        dependsOn.remove(inherit)
    }

    fun setPackage(umlPackage: UmlPackage) {
        this.umlPackage = umlPackage
    }
}
