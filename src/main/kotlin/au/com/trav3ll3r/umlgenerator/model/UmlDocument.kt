package au.com.trav3ll3r.umlgenerator.model

class UmlDocument {

    val packages = mutableListOf<UmlPackage>()

    fun addClass(packageName: String, umlClass: UmlClass): UmlClass? {
//        println("addClass | <$packageName>.<${umlClass.simpleName}>")
        return getOrCreate(packageName)?.let {
            umlClass.setPackage(it)
            it.addClass(umlClass)
            umlClass
        }
    }

    private fun getOrCreate(packageName: String): UmlPackage? {
        var result = findPackage(packageName)
        if (result != null) {
            return result
        } else {
            val tokens = packageName.split('.')
            val partialPackage = StringBuilder("")
            tokens.forEach {
                partialPackage.append(it)
                val existing = findPackage(partialPackage.toString())
                if (existing == null) {
                    result = createPackage(partialPackage.toString())
                }
                partialPackage.append(".")
            }
        }
        return result
    }

    private fun findPackage(name: String): UmlPackage? {
        // println("findPackage <$name>")
        var needle: UmlPackage? = null
        // println("findPackage <$name> | ${packages.size} top-level package(s)")
        packages.forEach {
            val findOutcome = it.findPackage(name)
            if (findOutcome != null) {
                needle = findOutcome
                return@forEach
            }
        }
        // println("findPackage <$name> | needle=$needle")
        return needle
    }

    private fun createPackage(name: String): UmlPackage? {
        var result: UmlPackage? = null
        // println("createPackage | $name")
        val parent: UmlPackage?
        val lastDotIndex = name.lastIndexOf('.')

        if (lastDotIndex > 0) {
            val parentName = name.substring(0, lastDotIndex)
            // println("looking for package <$parentName>")
            parent = findPackage(parentName)
            // println("<$name> parent is <$parentName>")
            if (parent != null) {
                // println("add <$name> under <$parentName>")
                result = UmlPackage(name)
                parent.addSubpackage(result)
            }
        } else {
            // println("add <$name> as root")
            result = UmlPackage(name)
            packages.add(result)
        }

        return result
    }
}
