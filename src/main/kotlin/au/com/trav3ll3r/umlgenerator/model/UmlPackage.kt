package au.com.trav3ll3r.umlgenerator.model

data class UmlPackage(val fullyQualifiedName: String) {

    val subpackages = mutableSetOf<UmlPackage>()
    val classes = mutableSetOf<UmlClass>()

    fun addSubpackage(subPackage: UmlPackage) {
        subpackages.add(subPackage)
    }

    fun addClass(umlClass: UmlClass) {
        classes.add(umlClass)
    }

    fun findPackage(name: String): UmlPackage? {
        if (fullyQualifiedName == name) {
            return this
        } else {
            subpackages.forEach { subpackage ->
                // RETURN AS SOON AS WE FIND FIRST NON-NULL
                subpackage.findPackage(name)?.also { return it }
            }
        }
        return null
    }

    fun isEmpty(): Boolean {
        if (classes.isEmpty() && subpackages.size < 2) {
            return true
        }
        return false
    }

    /* region render */
//    fun render(config: GeneratorConfig, depth: Int = 0): String {
//        val nextDepth = depth + 2
//        val indent = " ".repeat(depth)
//        val sb = StringBuilder()
//
//        val opening = "${indent}package $fullyQualifiedName {\n"
//        val closing = "${indent}}\n"
//
//        if (!isEmpty()) {
//            sb.append(opening)
//        }
//
//        // RENDER SUBPACKAGES
//        subpackages.forEach { sb.append(it.render(config, nextDepth)) }
//
//        // RENDER CLASSES
//        classes.forEach { sb.append(it.render(config, nextDepth)) }
//
//        if (!isEmpty()) {
//            sb.append(closing)
//        }
//
//        return sb.toString()
//    }
    /* endregion */

    /* region IDE generated */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as UmlPackage

        if (fullyQualifiedName != other.fullyQualifiedName) return false

        return true
    }

    override fun hashCode(): Int {
        return fullyQualifiedName.hashCode()
    }
    /* endregion generated */
}
