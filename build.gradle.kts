import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.5.31"
    application
}

group = "au.com.trav3ll3r"
version = "0.1-SNAPSHOT"

repositories {
    mavenCentral()
    // Kotlinx-Ast
    maven(url = "https://jitpack.io")
}

// Kotlinx-Ast
// https://github.com/kotlinx/ast
dependencies {
    // provides a kotlin ast parsed using antlr-kotlin
    api("com.github.kotlinx.ast:grammar-kotlin-parser-antlr-kotlin:ad21ce6141")
    // provides an antlr4 grammar ast parser using antlr-java
    //api("kotlinx.ast:grammar-antlr4-parser-antlr-java:ad21ce6141")

    api("com.github.javaparser:javaparser-core:3.23.0")
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test:1.5.31")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "11"
}

application {
    mainClass.set("au.com.trav3ll3r.umlgenerator.runner.GeneratorRunnerKt")
}
